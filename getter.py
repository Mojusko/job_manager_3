import time
import paramiko
import os
import subprocess
import argparse
import sqlite3 as lite
import sys

def get_jobs(con,settings):
	cur = con.cursor()	
	command=""
	ids=[]
	total_len=len(settings)
	counter=0
	#SPECIAL COOMANDS
	if settings[0]=="ALL":
		command=""
	else:
		for obj in settings:
			counter+=1
			obj2=obj.split("=")
			if ":" in obj2[1]:
				numbers=obj2[1].split(":")
				command=command+" "+obj2[0]+" BETWEEN "+numbers[0]+" AND "+numbers[1]
			else:
				if "u." in obj2[1]:
					obj3=obj2[1].split("u.")[1] 
					command=command+" "+obj2[0]+"=\'"+obj3+"\' "
				else:
					command=command+" "+obj2[0]+"="+obj2[1]+" "
			if counter<total_len:
				command=command+" AND "
		command=" WHERE " + command
	print "SELECT COMMAND: SELECT * FROM Jobs "+ command
		
	cur.execute("SELECT * FROM Jobs " + command)
	rows = cur.fetchall()
	for row in rows:
		ids.append(row)
	con.commit()
	return ids


def update_jobs(con,joblist,settings):
	cur = con.cursor()	
	command=""
	counter=0
	settings=settings.split(" ")
	total_len=len(settings)
	print settings
	for obj in settings:
		counter+=1
		obj2=obj.split("=")
		if ":" in obj2[1]:
			numbers=obj2[1].split(":")
			command=command+" "+obj2[0]+" BETWEEN "+numbers[0]+" AND "+numbers[1]
		else:
			if "u." in obj2[1]:
				obj3=obj2[1].split("u.")[1] 
				command=command+" "+obj2[0]+"=\'"+obj3+"\' "
			else:
				command=command+" "+obj2[0]+"="+obj2[1]+" "
		if counter<total_len:
				command=command+","
	command=" SET " + command

	for job in joblist:
		print "UPDATE COMMAND: UPDATE Jobs "+ command + "WHERE ID=",job[0] 
		cur.execute("UPDATE Jobs "+ command + "WHERE ID="+str(job[0]))
		con.commit()
	return 1

def print_jobs(id_list):

	total=0
	#for row in id_list:
	#	print row
	#"ID INTEGER PIMARY KEY, NAME TEXT, TIME_CREATION TEXT, TIME_HANDLING TEXT, SERVER TEXT, SERVER_SUB TEXT, USER TEXT, STATUS TEXT, TYPE INT, PRIORITY INT, CORES INT, PROCESS INT, NOTES TEXT, WORKING_DIR TEXT, OPTIONS TEXT);")
	# 0							1 			2					3					4			5 				6			7			8			9			10 			11			12			13					14			
	print '%5s' % "ID","|",'%10s' % "NAME","|",'%10s' % "TIME","|",'%10s' % "SERVER_SUB","|",'%12s' % "SERVER_HAND","|",'%12s' % "USER","|",'%7s' % "STATUS","|",'%8s' % "PRIORITY","|",'%5s' % "TYPE","|",'%8s' % "PROCESS","|"
	print '____________________________________________________________________________________________________________________________________'
	for row in id_list:
		print '%5s' % row[0],"|",'%10s' % row[1],"|",'%10s' % row[3],"|",'%10s' % row[5],"|",'%12s' % row[4],"|",'%12s' % row[6],"|",'%7s' % row[7],"|",'%8s' % row[9],"|",'%5s' % row[8],"|",'%8s' % row[11],"|"
		total+=1
	print "Total Jobs",total

def delete_jobs(con,joblist):

	total=0
	cur = con.cursor()	
	for job in joblist:
		print "DELETE FROM Jobs WHERE ID="+str(job[0])	
		cur.execute("DELETE FROM Jobs WHERE ID="+str(job[0]))
	con.commit()	