import hashlib
import time
import paramiko
import os
import subprocess
import argparse
import sqlite3 as lite
import sys
import numpy as np
import getpass
import socket

def submit_jobs(con,arglist,hash_file):
	copies=0
	try:
		script=arglist[0]
		group=int(arglist[1])
		user=getpass.getuser()
		#server_sub=socket.gethostbyname(socket.gethostname())
		server_sub="."
		try:
			name=arglist[2]
		except:
			name="UNK"
		try:
			copies=int(arglist[3])
		except:
			copies=1
		try:
			priority=int(arglist[4])
		except:
			priority=0
		try:
			working_dir=arglist[5]
		except:
			working_dir=os.getcwd()	
		try:
			cores=arglist[6]
		except:
			cores=-1
		print "NOTE: ADDING JOBS"
		cur = con.cursor()	
		for i in np.arange(0,copies,1):
			cur.execute("INSERT INTO Jobs (NAME,TYPE,PRIORITY,WORKING_DIR,CORES,STATUS,USER,SERVER_SUB) VALUES (\'"+str(name)+"\',"+str(group)+","+str(priority)+",\'"+str(working_dir)+"\',"+str(cores)+",\'"+str("unf")+"\',\'"+str(user)+"\',\'"+str(server_sub)+"\')")
			lid = cur.lastrowid
			m = hashlib.md5()
			print "Hashing", str(lid)
			m.update(str(lid))
			hash_script=m.hexdigest()
			print hash_script
			os.system("cp "+str(script)+" "+str(hash_file)+"/"+str(hash_script)+".sh")
		print "OK: ALL JOBS WERE SUCESSFULLY SUBMITTED"
	except: 
		Error="ERR: Give at least Script name and group ID"
	con.commit()