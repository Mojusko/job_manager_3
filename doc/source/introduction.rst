====================
Introduction
====================

JP Manager is a simple job manager with very intuitive and simple interface. It includes main console interface, however GUI version of job tracker can also be used.

JP Manager can be used in case you have different machines in your cloud or beowulf cloud, or even in case you are using servers across the globe. We developed JP Manager with intention to make it serve on cluster of Virtual Machines. Depending on your needs you might requiere shared drive, and SQL server but otherwise the limitations for usage of JP Manager are minimal. 

JP Manager is wholly written in Python and supported via SQL database to store job entries, thus compatibility issues are unlikely to appear. 

We support clever job distribution with various optional descriptions of jobs such as priority, working directories, and many other to make execution jobs in cloud as comfortable as possible.

JP Manager supports connection via SSH to a server of choice and exectuing jobs there. This allows even remote servers to be included in cloud, and offers security. In case, you prefer docer interface *link*, one is able to create its own docker bundle and send it along with a job needed to be performed. 

JP Manager resembles Git (version control system) in its interfaces, and can be used equally as confortable as Git.
