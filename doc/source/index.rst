.. JP Manager documentation master file, created by
   sphinx-quickstart on Thu Aug 28 11:57:46 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to JP Manager's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2
   
   introduction 
   install 
   setup
   basicusage


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

