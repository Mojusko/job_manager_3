"""
:Authors:
    Mojmir Mutny
:Version: 0.3 AUG 2014
:Dedication: To my father. 
"""
import time
import paramiko
import os
import subprocess
import argparse
import sqlite3 as lite
import sys
import numpy as np
from helper import *
import os 
import MySQLdb
import ConfigParser
from getter import *
from submiter import *
from sender import *

PATH=repr(os.getcwd()).split("'")[1]+"/config.ini"

parser = argparse.ArgumentParser(description='Job Manager Python (JP Manager) ver. 0.3 by Mojmir Mutny, University of Edinburgh')
parser.add_argument('-start',  nargs= '*', help='Prints currently assigned processes')
parser.add_argument('-connect', nargs= '*', help='Prints finished processes')
parser.add_argument('-init',  nargs= '*', help='Print currently running processes')
parser.add_argument('-set',  nargs= '*',help='Print currently running processes')

parser.add_argument('-list',  nargs= '*',help='Print currently running processes')
parser.add_argument('-kill',  nargs= '*',help='Print currently running processes')
parser.add_argument('-delete',  nargs= '*',help='Print currently running processes')
parser.add_argument('-reset',  nargs= '*',help='Print currently running processes')

parser.add_argument('-sub',  nargs= '*',help='Print currently running processes')
parser.add_argument('-send',  nargs= '*',help='Print currently running processes')

parser.add_argument('-dumpconf',action="store_true")
parser.add_argument('-dumplog', action="store_true")
args = parser.parse_args()


#################################################################
#### MODULE TO TEST DATABASE CONNECTION #########################
#################################################################
if args.connect is not None:
	if args.connect[0]=="web":
		host=ConfigSectionMap(PATH)["host"]
		user=ConfigSectionMap(PATH)["user"]
		passwd=ConfigSectionMap(PATH)["passwd"]
		dbb=ConfigSectionMap(PATH)["database"]
		try:
			db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbb)
			cursor = db.cursor()
			db.close()
			print "Database exists"
			ConfigSectionSet(PATH,"mode","web")
		except:
			print "ERR: Not found"
	elif args.connect[0]=="file":
		db = ConfigSectionMap(PATH)["database"];
		if os.path.isfile(db):
			print "File Exists"
			ConfigSectionSet(PATH,"mode","file")
		else:
			print "ERR: Not found"
			print "NOTE: Creating"
			os.system("touch "+db)
			ConfigSectionSet(PATH,"mode","file")
			
	else:
		print "ERR: Wrong Input."

#################################################################
#### MODULE TO CREATE A JOB TABLE IN DATABASE ###################
#################################################################
if args.init is not None:
	mode=ConfigSectionMap(PATH)["mode"];
	if mode=="web":
		host=ConfigSectionMap(PATH)["host"]
		user=ConfigSectionMap(PATH)["user"]
		passwd=ConfigSectionMap(PATH)["passwd"]
		dbb=ConfigSectionMap(PATH)["database"]
		#-----------------------------------------
		db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbb)
		cur = db.cursor() 
		cur.execute("CREATE TABLE Jobs(ID int NOT NULL, NAME varchar(255), TIME_CREATION varchar(255), TIME_HANDLING varchar(255), SERVER varchar(255), SERVER_SUB varchar(255), USER varchar(255), STATUS varchar(255), TYPE int, PRIORITY int, CORES int, PROCESS int, NOTES varchar(255),  WORKING_DIR varchar(255),OPTIONS varchar(255),PRIMARY KEY (ID))")
		db.close()
	elif mode=="file":
		dbb=ConfigSectionMap(PATH)["database"]
		db=lite.connect(dbb)
		cur = db.cursor() 
		cur.execute("CREATE TABLE Jobs( ID INTEGER PRIMARY KEY, NAME TEXT, TIME_CREATION TEXT, TIME_HANDLING TEXT, SERVER TEXT, SERVER_SUB TEXT, USER TEXT, STATUS TEXT, TYPE INT, PRIORITY INT, CORES INT, PROCESS INT, NOTES TEXT, WORKING_DIR TEXT, OPTIONS TEXT);")
		db.close()

#################################################################
#### MODULE TO SET CONFIG VARIABLES #############################
#################################################################
if args.set is not None:
	Config=ConfigParser.SafeConfigParser()
	Config.read(PATH)
	cfgfile = open(PATH,'w')
	for settings in args.set:
		rule=settings.split("=")
		Config.set("General",rule[0],rule[1])
	Config.write(cfgfile)
	cfgfile.close()	
#################################################################
#### MODULE TO DUMP CONFIG ######################################
#################################################################
if args.dumpconf==True:
	Config=ConfigParser.SafeConfigParser()
	Config.read(PATH)
	Config.write(sys.stdout)

#################################################################
#### MODULE TO LIST JOBS ########################################
#################################################################
if args.list is not None:
	mode=ConfigSectionMap(PATH)["mode"];
	if mode=="web":
		host=ConfigSectionMap(PATH)["host"]
		user=ConfigSectionMap(PATH)["user"]
		passwd=ConfigSectionMap(PATH)["passwd"]
		dbb=ConfigSectionMap(PATH)["database"]
		#-----------------------------------------
		db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbb)
	elif mode=="file":
		dbb=ConfigSectionMap(PATH)["database"]
		db=lite.connect(dbb)
	jobs=get_jobs(db,args.list)
	print_jobs(jobs)
	db.close()


#################################################################
#### MODULE TO DELETE JOBS ######################################
#################################################################
if args.delete is not None:
	mode=ConfigSectionMap(PATH)["mode"];
	if mode=="web":
		host=ConfigSectionMap(PATH)["host"]
		user=ConfigSectionMap(PATH)["user"]
		passwd=ConfigSectionMap(PATH)["passwd"]
		dbb=ConfigSectionMap(PATH)["database"]
		#-----------------------------------------
		db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbb)
	elif mode=="file":
		dbb=ConfigSectionMap(PATH)["database"]
		db=lite.connect(dbb)
	jobs=get_jobs(db,args.delete)
	delete_jobs(db,Ejobs)
	db.close()



#################################################################
#### MODULE TO SUBMIT JOBS ######################################
#################################################################
if args.sub is not None:
	mode=ConfigSectionMap(PATH)["mode"];
	if mode=="web":
		host=ConfigSectionMap(PATH)["host"]
		user=ConfigSectionMap(PATH)["user"]
		passwd=ConfigSectionMap(PATH)["passwd"]
		dbb=ConfigSectionMap(PATH)["database"]
		#-----------------------------------------
		db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbb)
	elif mode=="file":
		dbb=ConfigSectionMap(PATH)["database"]
		db=lite.connect(dbb)
	hash_file=ConfigSectionMap(PATH)["hashfile"]
	submit_jobs(db,args.sub,hash_file)
	db.close()

#################################################################
#### MODULE TO SEND JOBS ########################################
#################################################################
if args.send is not None:
	mode=ConfigSectionMap(PATH)["mode"];
	if mode=="web":
		host=ConfigSectionMap(PATH)["host"]
		user=ConfigSectionMap(PATH)["user"]
		passwd=ConfigSectionMap(PATH)["passwd"]
		dbb=ConfigSectionMap(PATH)["database"]
		#-----------------------------------------
		db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbb)
	elif mode=="file":
		dbb=ConfigSectionMap(PATH)["database"]
		db=lite.connect(dbb)
	hash_file=ConfigSectionMap(PATH)["hashfile"]
	logs_file=ConfigSectionMap(PATH)["logsfile"]	
	temp_file=ConfigSectionMap(PATH)["tempfile"]
	root_path=ConfigSectionMap(PATH)["rootpath"]
	jobs=get_jobs(db,args.send)
	print "IP Adress of the Server"
	#ip = raw_input()
	ip="localhost"
	print "User Name"
	#user= raw_input()
	user="mojko"
	print "Port [Default 22]"
	#port = raw_input()
	port=22
	send_jobs_manual(db,jobs,hash_file,logs_file,temp_file,root_path,ip,user,port)
	db.close()
