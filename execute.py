import time
import paramiko
import os
import subprocess
import argparse
import sqlite3 as lite
import sys
import numpy as np
from helper import *
import os 
import MySQLdb
import ConfigParser
from getter import *
from submiter import *
from sender import *
import subprocess

print "Debug 1"
PATH=sys.argv[2]+"/config.ini"
#GET ARGUMENT
print PATH
print "Debug 2"
temp=sys.argv[1]
print "Debug 3"
logs_file=ConfigSectionMap(PATH)["logsfile"]
#LOAD TEMP FILE
print "Debug 4"
f=open(temp,"r")
print "STARTING",time.time()
#CONNECT TO DATABASE
for job_id in f:
	print job_id
	log=open(logs_file+"/log_"+str(job_id[0])+".txt","w")
	mode=ConfigSectionMap(PATH)["mode"];
	if mode=="web":
		host=ConfigSectionMap(PATH)["host"]
		user=ConfigSectionMap(PATH)["user"]
		passwd=ConfigSectionMap(PATH)["passwd"]
		dbb=ConfigSectionMap(PATH)["database"]
		#-----------------------------------------
		db = MySQLdb.connect(host=host, user=user, passwd=passwd, db=dbb)
	elif mode=="file":
		dbb=ConfigSectionMap(PATH)["database"]
		db=lite.connect(dbb)
		con=db
	#SELECT JOBS
	cur = con.cursor()
	cur.execute("SELECT * FROM Jobs WHERE ID="+str(job_id))	
	rows = cur.fetchall()
	row=rows[0]
	print row
	con.commit()
	#UPDATE TO RUNNING
	update_jobs(con,rows,"STATUS=u.run")
	#CD TO DIRECTORY
	print "cd", str(row[13])
	os.chdir(str(row[13]))
	#Get the hash of the script
	m = hashlib.md5()
	print "HASHING", str(row[0])
	m.update(str(row[0]))
	hash_script=m.hexdigest()
	print hash_script
	#Execute it there
	hash_file=ConfigSectionMap(PATH)["hashfile"]
	print "Executing","sh",hash_file+"/"+str(hash_script)+".sh"
	proc = subprocess.Popen(["sh " +hash_file+"/"+str(hash_script)+".sh"], stdout=subprocess.PIPE, shell=True)
	(out, err) = proc.communicate()
	print "Debug 6"
	log.write(out)
	print('PID is: ' + str(proc.pid))
	update_jobs(con,rows,"PROCESS="+str(proc.pid))
	proc.wait()
	if err is None:
		update_jobs(con,rows,"STATUS=u.fin")
	else:
		update_jobs(con,rows,"STATUS=u.err")
		log.write(err)
	con.commit()
	log.close()

f.close()