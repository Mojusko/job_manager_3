import hashlib
import time
import paramiko
import os
import subprocess
import argparse
import sqlite3 as lite
import sys
import numpy as np
import getpass
import socket
from getter import *

def send_jobs_manual(con,arglist,hash_file,logs_file,temp_file,PATH,server,user,port):
	#Create a TEMP FILE where job ids are send
	name=temp_file+"/"+str(time.time())+".txt"
	temp=open(temp_file+"/"+str(time.time())+".txt","w")
	for job in arglist:
		temp.write(str(job[0])+"\n")
	temp.close()
	#Assign Jobs to the Server
	update_jobs(con,arglist,"STATUS=u.asi SERVER=u."+str(server))
	#Connect to Server Via SSH
	print "========= SENDING JOBS ON THE SERVER ======== "
	paramiko.util.log_to_file('ssh.log') 
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(server,username=user,port=port)
	print "nohup python "+PATH+"/execute.py "+str(name)+" "+ PATH+"  > " + logs_file+"/nohup.out"
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command( "nohup python "+PATH+"/execute.py "+str(name)+" " + PATH+ "> " + logs_file+"/nohup.out")
	ssh_stdin.flush()
	ssh.close()
	print "=========        JOBS SENT           ======== "


def send_jobs_auto(con,arglist,hash_file,logs_file,temp_file,ip,username,port):
	#Create a TEMP FILE where job ids are send
	temp=open(temp_file+"/"+str(time.time())+".txt","w")
	for job in arglist:
		temp.write(str(job[0])+"\n")
	temp.close()
